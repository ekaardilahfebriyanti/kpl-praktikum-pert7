package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import sun.misc.Unsafe;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.HashMap;

import static spark.Spark.get;
import static sun.misc.Unsafe.getUnsafe;

public class App {
    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);
//            Unsecure
//
//        Secure
        get("/latihan1", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            long start = System.currentTimeMillis();
            try{
                Thread.sleep(2000);
                model.put("a",System.currentTimeMillis()-start);
            } catch (InterruptedException e){
                Thread.currentThread().interrupt(); //reset status interrupt
            }
            String templatePath = "/view/form1.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

//        Unsecure
        Connection c;
        Statement statement;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver" );
            c = DriverManager.getConnection(
                    "jdbc:hsqldb:mem:usersdb", "SA", "");
            statement = c.createStatement();
            statement.execute("CREATE TABLE users(username varchar(20), password varchar(8));");
            statement.execute("INSERT INTO users values ('admin', 'admin')");
        } catch (Exception e) {
            System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
            e.printStackTrace();
            return;
        }
        get("/login", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String username = req.queryParamOrDefault("username", "");
            String password = req.queryParamOrDefault("password", "");
            if (username.isEmpty() && password.isEmpty()) {
                model.put("message", "");
            } else {
                String sql = "SELECT * FROM users WHERE username='"+username+"' AND password='"+password+"'";
                try {
                    ResultSet resultSet = statement.executeQuery(sql);
                    if (resultSet.next()) {
                        model.put("message", "Login berhasil");
                    } else {
                        model.put("message", "Login gagal");
                    }
                }catch (SQLException se) {
//                    System.out.println("Sql : "+ se);
//                    se.printStackTrace();
                    System.out.println("kesalahan pada sintaks SQL");
                    model.put("message", "Login Gagal");
                }
            }
            String templatePath = "/view/login.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

//        Latihan2
//        get("/latihan2", (req,res) -> {
//            HashMap<String, Object> model = new HashMap<>();
//            try{
//                NewInstance.undeclaredThrow(new IOException("Any Checked exception"));
//            }catch(Throwable e){
//                if (e instanceof IOException){
//                    model.put("a","IOException occured" );
//                } else if(e instanceof RuntimeException){
//                    throw (RuntimeException) e;
//                }else{
//                }
//                model.put("a", "there is none exception");
//            }
//            String templatePath = "/view/latihan2.vm";
//            return velocityTemplateEngine.render(new ModelAndView(model,
//                    templatePath));
//        });

//        Secure
        get("/latihan2", (req,res) -> {
            HashMap<String, Object> model = new HashMap<>();
            try{
                NewInstance.undeclaredThrow(
                        new IOException());
                model.put("a", "Any Checked exception");
            }catch(Throwable e){
                if (e instanceof IOException){
                    model.put("a","IOException occured" );
                } else if(e instanceof RuntimeException){
                    throw (RuntimeException) e;
                }else{}
                model.put("a", "there is none exception");
            }
            String templatePath = "/view/latihan2.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });
    }

//    public static class UnsafeSample{
//        public void methodWithNoDeclaredException(){
//            Unsafe unsafe = getUnsafe();
//            unsafe.throwException(new Exception("this should be check"));
//        }
//        private Unsafe getUnsafe(){
//            try{
//                Field field = Unsafe.class.getDeclaredField("theUnsafe");
//                field.setAccessible(true);
//                return (Unsafe) field.get(null);
//            }catch(Exception e){
//                throw  new RuntimeException(e);
//            }
//        }
//
//        public static void main(String[] args) {
//            new UnsafeSample().methodWithNoDeclaredException();
//        }
//    }
}
