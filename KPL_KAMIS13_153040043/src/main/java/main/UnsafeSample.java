package main;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class UnsafeSample {
    public void methodWithNoDeclaredException(){
        try{
            Unsafe unsafe = getUnsafe();
        }catch (Throwable e){
            if(e instanceof Exception) {
//            unsafe.throwException(new Exception("this should be check"));
                System.out.println("terjadi exception");
            }else {
                System.out.println("exception lain");
            }
        }
    }
    private Unsafe getUnsafe(){
        try{
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            return (Unsafe) field.get(null);
        }catch(Exception e){
            throw  new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        new UnsafeSample().methodWithNoDeclaredException();
    }
}

