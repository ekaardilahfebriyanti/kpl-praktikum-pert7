package main;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class NewInstance {
//    private static Throwable throwable;
//    private NewInstance() throws Throwable{
//        throw throwable;
//    }
//    public static synchronized void undeclaredThrow(Throwable throwable){
//        if(throwable instanceof IllegalAccessException || throwable
//                instanceof InstantiationError){
//            throw new IllegalArgumentException();
//        }
//        NewInstance.throwable = throwable;
//        try{
//            NewInstance.class.newInstance();
//        }catch(InstantiationException e){
//            System.out.println(e);
//        }catch (IllegalAccessException e){
//            System.out.println(e);
//        } finally {
//            NewInstance.throwable = null;
//        }
//    }

    private static Throwable throwable;
    private NewInstance() throws Throwable{
        throw throwable;
    }
    public static synchronized void undeclaredThrow(Throwable throwable){
        if(throwable instanceof IllegalAccessException || throwable
                instanceof InstantiationError){
            throw new IllegalArgumentException();
        }
        NewInstance.throwable = throwable;
        try{
            Constructor constructor = NewInstance.class.getConstructor(new
                    Class<?>[0]);
            constructor.newInstance();
        }catch(InstantiationException e){

        }catch (IllegalAccessException e) {
        }catch (InvocationTargetException e) {
            System.out.println("Exception thrown : "+e.getCause().toString());
        } catch (NoSuchMethodException e) {
        }finally {
            NewInstance.throwable = null;
        }
    }
}
